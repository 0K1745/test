import {randomUUID} from "crypto";

export class User {
    constructor(name: string, age: number) {
        if (!age || age <= 0) {
            throw new Error('age is not set and should be')
        }
        if (!name || name.length <= 0) {
            throw new Error('name is not set and should be')
        }
        this._age = age;
        this._name = name;

        this._id = randomUUID();
    }


    private _name: string;
    private _id: string;
    private _age: number;


    get name(): string {
        return this._name;
    }

    get id(): string {
        return this._id;
    }

    get age(): number {
        return this._age;
    }
}