import {describe, expect, it} from '@jest/globals';

import {UserService} from "../../src/services/userService";
import {User} from "../../src/domain/user";

describe("User", () => {
    it("should create a user with a name and an age", async () => {
        const userService = new UserService();
        const userToBeCreate = new User('name', 23);
        const userExpected = await userService.create(userToBeCreate);

        expect(userExpected.name).toEqual(userToBeCreate.name);
        expect(userExpected.age).toEqual(userToBeCreate.age);
        expect(userExpected.id).not.toBeNull();
    })

    it("should not create a user when there is no age", () => {
        const creationCall = () => {
            // @ts-ignore
            new User('name', undefined);
        }
        expect(creationCall).toThrow(new Error('age is not set and should be'));

    })

    it("should not create a user when there is no age", () => {
        const creationCall = () => {
            // @ts-ignore
            new User('name', 0);
        }
        expect(creationCall).toThrow(new Error('age is not set and should be'));

    })

    it("should not create a user when there is no name", () => {
        const creationCall = () => {
            // @ts-ignore
            new User(undefined, 23);
        }
        expect(creationCall).toThrow(new Error('name is not set and should be'));

    })


    it("should not create a user when there is an empty name", () => {
        const creationCall = () => {
            // @ts-ignore
            new User("", 23);
        }
        expect(creationCall).toThrow(new Error('name is not set and should be'));

    })

});